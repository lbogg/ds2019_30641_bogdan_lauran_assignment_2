package com.rabitmq.messsage.creator;

import com.jayway.jsonpath.spi.json.GsonJsonProvider;

import java.time.LocalDateTime;

public class Patient {
    private String id;
    private String activity;
    private String start;
    private String end;

    public Patient(String id, String start, String end, String activity) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString(){
        GsonJsonProvider gsonJsonProvider = new GsonJsonProvider();
        return gsonJsonProvider.toJson(this);
    }
}
