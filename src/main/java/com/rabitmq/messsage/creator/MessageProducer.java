package com.rabitmq.messsage.creator;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.impl.AMQChannel;
import net.minidev.json.JSONObject;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

public class MessageProducer{

    public static void main(String[] argv) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("Default",false,false,false,null);
        File file = new File("src/activity.txt");
        Scanner read;
        JSONObject jsonObject = new JSONObject();
        {
            try {
                read = new Scanner(file);
                while (read.hasNextLine()) {
                    String line = read.nextLine();
                    Scanner lineReader = new Scanner(line);
                    lineReader.useDelimiter("\t\t");
                    while (lineReader.hasNext()) {
                        Patient patient = new Patient(UUID.randomUUID().toString(), lineReader.next(), lineReader.next(), lineReader.next());
                        jsonObject.appendField("patient_id", patient.getId());
                        jsonObject.appendField("activity", patient.getActivity());
                        jsonObject.appendField("start", patient.getStart());
                        jsonObject.appendField("end", patient.getEnd());
                        //System.out.println(jsonObject);
                        channel.basicPublish("","Default",null,jsonObject.toString().getBytes());
                    }
                    lineReader.close();
                }
                read.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
