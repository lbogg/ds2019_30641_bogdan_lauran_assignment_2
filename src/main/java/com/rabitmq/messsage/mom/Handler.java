package com.rabitmq.messsage.mom;

import com.rabbitmq.client.*;
import com.rabitmq.messsage.creator.MessageProducer;
import jdk.nashorn.internal.parser.JSONParser;
import jsonTuples.Parser;
import net.bytebuddy.asm.Advice;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeoutException;

public class Handler {

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare("Default",false,false,false,null);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(),"UTF-8");
            jsonTuples.JSONObject jsonObject = (jsonTuples.JSONObject) Parser.parse(message);
            LocalDateTime dateTimeStart = LocalDateTime.parse(jsonObject.get("start").toString(), formatter);
            LocalDateTime dateTimeEnd = LocalDateTime.parse(jsonObject.get("end").toString(), formatter);
            long d = Duration.between(dateTimeStart, dateTimeEnd).toHours();
            if(jsonObject.get("activity").equals("Sleeping") && d > 12){
                System.out.println("Sleep period longer then 12 hours for patientId="+jsonObject.get("patient_id"));
            }
            if(jsonObject.get("activity").equals("Leaving") && d > 12){
                System.out.println(" The leaving activity (outdoor) is longer than 12 hours for patientId="+jsonObject.get("patient_id"));
            }
            if((jsonObject.get("activity").equals("Toileting") || jsonObject.get("activity").equals("Grooming") || jsonObject.get("activity").equals("Showering") || jsonObject.get("activity").equals("Toileting")) && d > 1){
                System.out.println("The period spent in bathroom is longer than 1 hour for patientId="+jsonObject.get("patient_id"));
            }
            System.out.println(" [x] Received '" + message + "'");
        };
        channel.basicConsume("Default",true, deliverCallback, consumerTag->{});

       /* channel.basicConsume("Default",false,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String routingKey = envelope.getRoutingKey();
                String contentType = properties.getContentType();
                long deliveryTag = envelope.getDeliveryTag();
                // (process the message components here ...)
                String message = new String(body,"UTF-8");
                jsonTuples.JSONObject jsonObject = (jsonTuples.JSONObject) Parser.parse(message);
                LocalDateTime dateTimeStart = LocalDateTime.parse(jsonObject.get("start").toString(), formatter);
                LocalDateTime dateTimeEnd = LocalDateTime.parse(jsonObject.get("end").toString(), formatter);
                long d = Duration.between(dateTimeStart, dateTimeEnd).toHours();

                if(jsonObject.get("activity").equals("Sleeping") && d > 12){
                    System.out.println("Sleep period longer then 12 hours for patientId="+jsonObject.get("patient_id"));
                }
                if(jsonObject.get("activity").equals("Leaving") && d > 12){
                    System.out.println(" The leaving activity (outdoor) is longer than 12 hours for patientId="+jsonObject.get("patient_id"));
                }
                if((jsonObject.get("activity").equals("Toileting") || jsonObject.get("activity").equals("Grooming") || jsonObject.get("activity").equals("Showering") || jsonObject.get("activity").equals("Toileting")) && d > 1){
                    System.out.println("The period spent in bathroom is longer than 1 hour for patientId="+jsonObject.get("patient_id"));
                }
                //System.out.println(d);
                channel.basicAck(deliveryTag, false);
            }
        });*/
    }
}
